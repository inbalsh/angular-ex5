import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//material angular
import {MatTableModule} from '@angular/material/table'; //API Table
//import {MatGridListModule} from '@angular/material/grid-list'; // API grid-list

//firebase modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { AppComponent } from './app.component';
import { MoviesComponent } from './movies/movies.component';
import { MovieComponent } from './movie/movie.component';

import {environment} from '../environments/environment'; // הוספת אימפורט
import { MatSelectModule } from '@angular/material/select'; // לתרגיל 5
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // לתרגיל 5

@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    MovieComponent
  ],
  imports: [
    BrowserModule,
    MatTableModule,  // יש להוסיף זאת לפי האימפורט למעלה
  //MatGridListModule, // לפי גריד ליסט
  AngularFireModule.initializeApp(environment.firebase), // הוספת אימפורט פיירבייס
  AngularFireDatabaseModule, // הוספת אימפורט פיירבייס
  AngularFireAuthModule, // הוספת אימפורט פיירבייס
  MatSelectModule, // לתרגיל 5
  BrowserAnimationsModule, // לתרגיל 5
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
