import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database' //הוספה

@Component({
  selector: 'movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']

})
export class MoviesComponent implements OnInit {

/*
  decmovies = [
    { 'id': '1', 'title': 'Spider-Man: Into The Spider-Verse', 'studio': 'Sony', 'weekendIncome': '$35,400,000' },
    { 'id': '2', 'title': 'The Mule', 'studio': 'WB', 'weekendIncome': '$17,210,000' },
    { 'id': '3', 'title': "Dr. Seuss' The Grinch (2018)", 'studio': 'Uni.', 'weekendIncome': '$11,580,000' },
    { 'id': '4', 'title': 'Ralph Breaks the Internet', 'studio': 'BV', 'weekendIncome': '$9,589,000' },
    { 'id': '5', 'title': 'Mortal Engines', 'studio': 'Uni.', 'weekendIncome': '$7,501,000' },
];
*/

displayedColumns: string[] = ['id', 'title', 'studio', 'weekendIncome', 'delete'];
  
movies = [];
studios = [];

name = 'No movies were deleted';
studio = "";

/*
  showTitle($event){
    this.titleFromMovie = $event;
  }
*/

  constructor(private db:AngularFireDatabase) // החיבור לעולם הפיירבייס
   {

    }

  ngOnInit() {
    this.db.list('/decmovies').snapshotChanges().subscribe( //השם בתוך הליסט זה בשם של הדטה בפיירבייס. במקרה זה decmovies
    // יצירת אובייקט מסוג אנגולר שנקרא דיבי, לאובייקט קיימת פונקציה שנקראת ליסט אשר קוראת מהדטה בייס רשימה של אובייקטים ומקבלת אנדפוינט בשם טודוס , סנאפשוט מילה שמורה שזו אובסרבבל מוכן לשימוש
      movies =>{
        this.movies = []; // מאפסים את כל טודוס, הפיכה למערך ריק. זה הטודוס שלנו שהגדרנו למעלה
        this.studios = ['all'];
        movies.forEach( // הפעלת הפונקציה פוראיצ' לכל הטודוס. זוהי לולאה שרצה על כל האובייקטים ברשימה - קי 
          movie => { // הפונקציה קוראת לכל אחד באופן זמני - טודו
            let y = movie.payload.toJSON(); // משתנה חדש בו נכניס את התוכן של קי כלומר הפיילוד והעברה לפורמט גייסון 
    //      y["$key"] = movie.key; // אנו נרצה שוואי יכיל גם את השדה קי ולכן כאן מבצעת הוספת שדה בו נכניס את הטודו נקודה קי -מילה שמורה
            this.movies.push(y); // הכנסת האיבר שיצרנו לתוך טודוס
            let stu = y['studio'];
            if (this.studios.indexOf(stu) == -1) {
              this.studios.push(y['studio']);
            }
        }
      )
      }
    )
  }

  
  toFilter() {
    let id = 1;
    this.db.list('/decmovies').snapshotChanges().subscribe(
      movies => {
        this.movies = [];
        movies.forEach(
          movie => {
            let y = movie.payload.toJSON();
            if (this.studio == 'all') {
              this.movies.push(y);
            }
            else if (y['studio'] == this.studio) {
              y['id'] = id;
              id++;
              this.movies.push(y);
            }
          }
        )
      }
    )
  }


toDelete(element)
{

  let start, end = this.movies;
  let id = element.id;
  let i = 0;
  let deleted = [];
    
  for(let movie of this.movies)
  {
    deleted[i] = movie.id;
    i++;
  }
  
  start = this.movies.slice(0,deleted.indexOf(id));
  end = this.movies.slice(deleted.indexOf(id)+1, this.movies.length+1);
  this.movies = start;
  this.movies = this.movies.concat(end);   
  this.name = element.title;

}


}
