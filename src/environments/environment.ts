// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCKivZscDNB_T5w69B0A-HlVl0QvntMj10",
    authDomain: "ex4-movies.firebaseapp.com",
    databaseURL: "https://ex4-movies.firebaseio.com",
    projectId: "ex4-movies",
    storageBucket: "ex4-movies.appspot.com",
    messagingSenderId: "292166354127"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
